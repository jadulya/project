package ru.inno.sockets.repositories;


import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.net.Socket;
import java.sql.*;

public class PlayerRepositoryImpl implements PlayerRepository {


    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from account where name = ?";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from account where id = ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into account (ip_address, name, max_pointer,win_count,defeat_count) values (?,?,?,?,?)";

    private DataSource dataSource;
    private RowMapper<Player> playerRowMapper = row ->
            Player.builder()
                    .playerId((long) row.getInt("id"))
                    .nickName(row.getString("name"))
                    .build();

    public PlayerRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getIpAddress());
            System.out.println(player.getIpAddress());
            statement.setString(2, player.getNickName());
            statement.setInt(3,0 );
            statement.setInt(4,0 );
            statement.setInt(5,0 );
            statement.executeUpdate();
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                player.setPlayerId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Player findByNickName(String nickName) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {

            statement.setString(1, nickName.toString());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return playerRowMapper.mapRow(result);
            }

            return null;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {

    }

    @Override
    public Player findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id.intValue());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return playerRowMapper.mapRow(result);
            }

            return null;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
