package ru.inno.sockets.repositories;


import ru.inno.sockets.models.Game;

import java.util.Optional;

public interface GameRepository {
    void save (Game game);
    Optional<Game> findById (Long gameId);
    void update (Game game);

}
