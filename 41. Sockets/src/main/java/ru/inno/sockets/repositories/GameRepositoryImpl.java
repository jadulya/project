package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class GameRepositoryImpl implements GameRepository {
    //language=SQL
   // private static final String SQL_INSERT = "insert into game (date, ) values (?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";
    //language=SQL
    private static final String SQL_INSERT = "insert into game (date, duration_game) values (?,?)";
    private DataSource dataSource;
    private PlayerRepository playerRepository;

    public GameRepositoryImpl(DataSource dataSource, PlayerRepository playerRepository) {
        this.dataSource = dataSource;
        this.playerRepository = playerRepository;
    }
    private RowMapper<Game> gameRowMapper = row ->
            Game.builder()
                    .gameId((long) row.getInt("id"))
                    .gameDate(row.getString("date"))
                    .build();

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getGameDate());
            statement.setInt(2,0);
            statement.executeUpdate();
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("id");
                game.setGameId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Optional<Game> findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, gameId.intValue());
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Optional.of(gameRowMapper.mapRow(result));
            }

            return Optional.empty();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }



    @Override
    public void update(Game game) {

    }
}
