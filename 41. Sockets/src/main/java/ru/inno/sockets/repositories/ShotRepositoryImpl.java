package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

public class ShotRepositoryImpl implements ShotRepository {
    public void save(Shot shot) {
        System.out.println("ShootRepository - saved " + shot.getShooter().getNickName() + " "
                + shot.getTarget().getNickName() + " with game id = " + shot.getGame().getGameId());
    }

}
