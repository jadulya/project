package ru.inno.sockets.repositories;


import ru.inno.sockets.models.Shot;

public interface ShotRepository {
    void save (Shot shot);
}
