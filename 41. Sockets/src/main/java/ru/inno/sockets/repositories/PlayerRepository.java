package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

public interface PlayerRepository {
    void save (Player player);
    Player findByNickName (String nickName);
    void update (Player player);
    Player findById (Long id);
}
