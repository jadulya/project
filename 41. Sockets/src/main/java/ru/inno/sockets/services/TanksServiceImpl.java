package ru.inno.sockets.services;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.models.Shot;
import ru.inno.sockets.repositories.*;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;


public class TanksServiceImpl implements TanksService {
    private DataSource dataSource;


    { HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/final_project");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("123NeLLy123");
        hikariConfig.setMaximumPoolSize(20);
        dataSource = new HikariDataSource(hikariConfig);
    }

   private ShotRepository shotRepository = new ShotRepositoryImpl();
    private PlayerRepository playerRepository = new PlayerRepositoryImpl(dataSource);
    private GameRepository gameRepository = new GameRepositoryImpl(dataSource, playerRepository);

    public Long startGame(String firstPlayerNickname, String secondPlayerNickname) {
        Player firstPlayer = playerRepository.findByNickName(firstPlayerNickname);
        Player secondPlayer = playerRepository.findByNickName(secondPlayerNickname);
        // если пользователи новые
        if (firstPlayer == null) {
            firstPlayer = new Player(firstPlayerNickname);
            playerRepository.save(firstPlayer);
        } else {
            // TODO: обновить IP
        }

        if (secondPlayer == null) {
            secondPlayer = new Player(secondPlayerNickname);
            playerRepository.save(secondPlayer);
        }else {
            // TODO: обновить IP
        }

        Game game = new Game();
        game.setFirst(firstPlayer);
        game.setSecond(secondPlayer);
         game.setGameDate(LocalDate.now().toString());
        gameRepository.save(game);
        return game.getGameId();
    }

    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playerRepository.findByNickName(shooterNickname);
        Player target = playerRepository.findByNickName(targetNickname);
        Game game = gameRepository.findById(gameId).get();
        Shot shot = new Shot(shooter, target, game);
        if (game.getFirst().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecond().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shotRepository.save(shot);
        gameRepository.update(game);
        System.out.println(" shots: " +
                game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());
    }
}
