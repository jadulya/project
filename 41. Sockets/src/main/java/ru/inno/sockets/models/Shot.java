package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Shot {
    private Player shooter;
    private Player target;
    private Game game;

    public Shot(Player shooter, Player target, Game game) {
        this.shooter = shooter;
        this.target = target;
        this.game = game;
    }

}
