package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Data
@AllArgsConstructor
@Builder
public class Player {

    private Long playerId;
    private String nickName;
    private String ipAddress;


    public Player(String nickName) {
        this.nickName = nickName;
    }


    public Player() {

    }
}
