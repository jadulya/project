package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Builder

public class Game {
    private Long gameId;
    private Player first;
    private Player second;
    private String gameDate;
    private Long gameTime;
    private Long durationGame;
    private Integer firstPlayerShotsCount = 0;
    private Integer secondPlayerShotsCount = 0;

    public Game() {

    }


}
